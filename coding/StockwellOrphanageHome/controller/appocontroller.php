<?php
require_once "connection.php";

Class appoController
{
	private $appointmentid;
	private $a_time;
	private $a_date;
	private $orphanid;
	private $userid;

	function __construct()
	{
		$this->connect=new Connection();
	}

	//getter
	public function getAppointmentId()
	{
		return $this->appointmentid;
	}
	public function getATime()
	{
		return $this->a_time;
	}
	public function getADate()
	{
		return $this->a_date;
	}
	public function getUserId()
	{
		return $this->userid;
	}
	public function getOrphanId()
	{
		return $this->orphanid;
	}

	//setter

	public function setAppointmentId($appointmentid)
	{
		$this->appointmentid=$appointmentid;
	}
	public function setATime($a_time)
	{
		$this->a_time=$a_time;
	}
	public function setADate($a_date)
	{
		$this->a_date=$a_date;
	}
	public function setUserId($userid)
	{
		$this->userid=$userid;
	}
	public function setOrphanId($orphanid)
	{
		$this->orphanid=$orphanid;
	}

	public function addAppointment()
	{
		$sql="INSERT INTO appointment VALUES ('$this->appointmentid','$this->a_time','$this->a_date','3','2')";
		RETURN $this->connect->queries($sql);
	}
	public function selectAppointment()
	{
		return $this->connect->getData("SELECT * FROM appointment");
	}

	public function showAppointment()
	{
		return $this->connect->getData("SELECT * FROM appointment where userid=3");
	}
	
}
?>


