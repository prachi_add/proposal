<?php
require_once ('connection.php');

class User
{
	private $userid;
	private $first_name;
    private $last_name;
    private $email;
    private $DOB; 
    private $address;
    private $phone;
    private $occupation;
    private $gender;
    private $username;
    private $password;
    private $usertype;

    function __construct()
    {
    	$this->connect= new Connection();
    }

    //getter
    public function getUserId()
    {
    	return $this->userid;
    }
    public function getFirstname()
    {
    	return $this->first_name;
    }
    public function getLastname()
    {
    	return $this->last_name;
    }
    public function getEmail()
    {
    	return $this->email;
    }
    public function getDOB()
    {
    	return $this->DOB;
    }
    public function getAddress()
    {
    	return $this->address;
    }
    public function getPhone()
    {
    	return $this->phone;
    }
    public function getOccupation()
    {
    	return $this->occupation;
    }
    public function getGender()
    {
    	return $this->gender;
    }
    public function getUsername()
    {
    	return $this->username;
    }
    public function getPassword()
    {
    	return $this->password;
    }
    public function getUsertype()
    {
        return $this->usertype;
    }
       //setter//
    public function setUserId($userid)
    {
    	$this->userid=$userid;
    }
    public function setFirstname($first_name)
    {
    	$this->first_name=$first_name;
    }
    public function setLastname($last_name)
    {
    	$this->last_name=$last_name;
    }
    public function setEmail($email)
    {
    	$this->email=$email;
    }
    public function setDOB($DOB)
    {
    	$this->DOB=$DOB;
    }
    public function setAddress($address)
    {
    	$this->address=$address;
    }
    public function setPhone($phone)
    {
    	$this->phone=$phone;
    }
    public function setOccupation($occupation)
    {
    	$this->occupation=$occupation;
    }
    public function setGender($gender)
    {
    	$this->gender=$gender;
    }
    public function setUsername($username)
    {
    	$this->username=$username;
    }
    public function setPassword($password)
    {
    	$this->password=$password;
    }
    public function setUsertype($usertype)
    {
        $this->usertype=$usertype;
    }

    public function selectuserdetails()
    {
        $sql="SELECT * from user where usertype='subscriber'";
        return $this->connect->getData($sql);
    }


    public function getUserByUsername()
    {
        $sql="select * from user where username='$this->username'";
        return $this->connect->getData($sql);
    }
    
    public function getUserByEmail()
    {       
        $sql="select * from user where email='$this->email'";
        return $this->connect->getData($sql);
    }

    public function getUserById()
    {
        $sql="select * from user where userid='$this->userid'";
        return $this->connect->getData($sql);
    }
    public function addUser()
    {
        $sql ="INSERT into user values ('$this->userid','$this->first_name','$this->last_name','$this->email','$this->DOB','$this->address','$this->phone','$this->occupation','$this->gender','$this->username','$this->password','subscriber')";
        return $this->connect->queries($sql);
    }
    public function editProfile()
    {
        $sql="UPDATE user SET
        first_name='$this->first_name',
        last_name='$this->last_name',
        email='$this->email',
        DOB='$this->DOB',
        address='$this->address',
        phone='$this->phone',
        occupation='$this->occupation',
        gender='$this->gender',username='$this->username',password='$this->password',usertype='subscriber'
        where userid
        ={$this->userid}";
        return $this->connect->queries($sql);
    }

    public static function getID($username)
    {
        $sql =" SELECT userid from user where username= '$username'";
        $connection = new Connection;
        $data = $connection->getData($sql);
        if (is_array($data)) 
        {
        $id = $data[0]['userid'];
        }
        else
        {
        $id = "No data found";
        }
        return $id;
        }

public function login()
{
    $rows = $this->getUserByUsername();
    $rowCount = $rows[0]['username'];
    
    if(!count($rowCount) > 0){
        $message = "Username doesn't exists";
        $_SESSION['loginerror']="Username doesnt exists";
    }else{
$sql=$this->connect->getData("SELECT * from user  WHERE username='$this->username' and password='$this->password'");

return $sql;
        }
}
    function DeleteUser($userid){
return $this->connect->queries("DELETE FROM user WHERE userid='$userid'");
}
public function selectUser($userid)
{
$sql="select * from user where userid = $userid ";
return $this->connect->getData($sql);
}
    public function register()
    {
        
        $rows = $this->getUserByEmail();
        
        if(count($rows) > 0){
            $message="Email already exists";
                $_SESSION['error'] = "Email already exists";
                return false;
                }else{
            if($this->addUser()){
                $rows = $this->getUserByEmail();
                                    foreach ($rows as $row) {
                        $email = $row['email'];
                                }
                        $_SESSION['email'] =$row['email'];
                            $_SESSION['userId'] =$row['id'];
                        return true;
                    }else{
                    return false;
                    }
    }
    
    
    }


}
?>