<?php
require_once "connection.php";

Class eventController
{
	private $eventid;
	private $event_title;
	private $description;
	private $time;
	private $date;
	private $duration;
	private $location;

	function __construct()
	{
		$this->connect=new Connection();
	}

	//getter
	public function getEventId()
	{
		return $this->eventid;
	}
	public function getEventTitle()
	{
		return $this->event_title;
	}
	public function getDescription()
	{
		return $this->description;
	}
	public function getTime()
	{
		return $this->time;
	}
	public function getDate()
	{
		return $this->date;
	}
	public function getDuration()
	{
		return $this->duration;
	}
	public function getLocation()
	{
		return $this->location;
	}

	//setter

	public function setEventId($eventid)
	{
		$this->eventid=$eventid;
	}
	public function setEventTitle($event_title)
	{
		$this->event_title=$event_title;
	}
	public function setDescription($description)
	{
		$this->description=$description;
	}
	public function setTime($time)
	{
		$this->time=$time;
	}
	public function setDate($date)
	{
		$this->date=$date;
	}
	public function setDuration($duration)
	{
		$this->duration=$duration;
	}
	public function setLocation($location)
	{
		$this->location=$location;
	}

	public function addEvent()
	{
		$sql="INSERT INTO event VALUES ('$this->eventid','$this->event_title','$this->description','$this->time','$this->date','$this->duration','$this->location')";
		return $this->connect->queries($sql);
	}

	function selectRow($eventid)
	{
		return $this->connect->getData("SELECT * FROM event WHERE eventid='$eventid'");
	}
	public function UpdateEvent()
	{
		$sql ="UPDATE event SET event_title='$this->event_title',description='$this->description',time='$this->time',date='$this->date',duration='$this->duration',location='$this->location' WHERE 
		 eventid={$this->eventid}";
			return $this->connect->queries($sql);	
	}

	public function selectEvent()
	{
		return $this->connect->getData("SELECT * FROM event");
	}

	function DeleteEvent($eventid)
	{
		$sql=("DELETE FROM event WHERE eventid='$eventid'");
		return $this->connect->queries($sql);
	}	
	
	
}
?>


