<?php
require_once "connection.php";

Class orphanController
{
	private $orphanid;
	private $orphan_name;
	private $orphan_gender;
	private $orphan_DOB;
	private $photo;

	function __construct()
	{
		$this->connect=new Connection();
	}

	public function getOrphanid()
	{
		return $this->orphanid;
	}
	public function getOrphanName()
	{
		return $this->orphan_name;
	}
	public function getOrphanGender()
	{
		return $this->orphan_gender;
	}
	public function getOrphanDOB()
	{
		return $this->orphan_DOB;
	}
	public function getPhoto()
	{
		return $this->photo;	
	}

	public function setOrphanid($orphanid)
	{
		$this->orphanid=$orphanid;
	}
	public function setOrphanName($orphan_name)
	{
		$this->orphan_name=$orphan_name;
	}
	public function setOrphanGender($orphan_gender)
	{
		$this->orphan_gender=$orphan_gender;
	}
	public function setOrphanDOB($orphan_DOB)
	{
		$this->orphan_DOB=$orphan_DOB;
	}
	public function setPhoto($photo)
	{
		$this->photo=$photo;	
	}

	public function addOrphan()
	{
		$sql="INSERT INTO orphan VALUES ('$this->orphanid','$this->orphan_name','$this->orphan_gender','$this->orphan_DOB','$this->photo')";
		return $this->connect->queries($sql);
	}

	function selectRow($orphanid)
	{
		return $this->connect->getData("SELECT * FROM orphan WHERE orphanid='$orphanid'");
	}

	public function UpdateOrphan()
	{
		$sql = "UPDATE orphan SET orphan_name='$this->orphan_name',orphan_gender='$this->orphan_gender',orphan_DOB='$this->orphan_DOB',photo='$this->photo' WHERE orphanid={$this->orphanid}";

			return $this->connect->queries($sql);	
	}

	public function selectOrphan()
	{
		return $this->connect->getData("SELECT * FROM orphan");
	}

	function DeleteOrphan($orphanid)
	{
		$sql=("DELETE FROM orphan WHERE orphanid='$orphanid'");
		return $this->connect->queries($sql);
	}	
	
}
?>