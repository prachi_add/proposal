
<?php
include('header.php');
require('../model/connection.php');
require('../controller/eventcontroller.php');
$eventcon=new eventController();
$data=$eventcon->selectEvent();
?>

<!--Carousel Wrapper-->
<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
    
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="view">
        <img class="d-block w-100" src="img/orphan4.jpg" width=100% height=700 alt="First slide">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Light mask</h3>
        <p>First text</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="img/orphan2.jpg" width=100% height=700  alt="Second slide">
        <div class="mask rgba-black-strong"></div>
      </div>
      <div class="carousel-caption">
        <h3 class="h3-responsive">Strong mask</h3>
        <p>Secondary text</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="img/orphan3.jpg" width=100% height=700 alt="First slide">
        <div class="mask rgba-black-light"></div>
      </div>
            <div class="carousel-caption">
        <h3 class="h3-responsive">Slight mask</h3>
        <p>Third text</p>
      </div>
    </div>
  </div>
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->
<hr>
<div class="container">

<section class="my-5">

  <!-- Section heading -->
  <h2 class="h1-responsive font-weight-bold text-center my-5">About Us</h2>
  <!-- Section description -->
  <p class="text-center w-responsive mx-auto mb-5">Stockwell Orphanage was founded in 1867 by Charles Spurgeon in what is now Stockwell Park Estate in London. </p>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
        <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/img%20(27).jpg" alt="Sample image">
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">
      <!-- Post title -->
     
      <!-- Excerpt -->
      <p>It opened on September 9 1869 and could accommodate 250 children at a cost of about £5,000 a year. The Orphanage was a home for boys without fathers between the ages of six and ten.  In 1879, the orphanage accommodation was expanded allowing girls to be admitted.The orphanage was open until the second World War.</p>
      <p>A warm welcome to the Former Children's Homes website. Having started in 2011, this is the first dedicated encyclopaedia of life in former children's homes and orphanages. Over the years, thousands and thousands of children spent time in these homes and yet we know very little about what life was like in them.</p>
        <p>Over the years, thousands and thousands of children spent time in these homes and yet we know very little about what life was like for them.
      </p>
      <!-- Read more button -->
      <a class="btn btn-default font-weight-bold btn-md" href="aboutus.php">Read more</a>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

</section>
<!-- Section: Blog v.1 -->
</div>
<hr>
<!--EVENTS-->
<div class="container">
  <h2 class="h1-responsive font-weight-bold text-center my-5">Events</h2>
  <!-- Grid row -->
<div class="row">
  <?php
    foreach($data as $key)
    {
  ?> 

<!-- Grid column -->
  <div class="col-lg-4 col-md-6">

    <!--Panel-->
    <div class="card text-center"">
    <div class=" card-header default-color white-text">
      Featured
    </div>
    <div class="card-body">
      <h4 class="card-title"><?php echo $key['event_title']?></h4>
      <p class="card-text"><?php echo $key['description']?></p>
      <a class="btn btn-default btn-sm" href="events.php">View More</a>
    </div>
    <div class="card-footer text-muted default-color white-text">
      <p class="mb-0">Date: <?php echo $key['date']?></p>
    </div>
  </div>
  <!--/.Panel-->
</div>
<!-- Grid column -->
  <?php
}
  ?>
</div>
<!-- Grid row -->
</div>

<hr>

<style>
.container p
{
  font-weight: bold;
  font-size: 13px;
  font-family: arial;
}
</style>
<?php
include('footer.php');
?>