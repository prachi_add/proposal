<?php
include('admindash.php');
include('../../controller/orphancontroller.php');



$orphancon=new orphanController();
$orphanid=$orphancon->selectRow($_GET['id']);

if (isset($_POST['editorphan']))
{
	$orphancon->setOrphanId($_GET['id']);
	$orphancon->setOrphanName($_POST['orphan_name']);
	$orphancon->setOrphanGender($_POST['orphan_gender']);
	$orphancon->setOrphanDOB($_POST['orphan_DOB']);
	$orphancon-> setPhoto($_POST['photo']);

	if($orphancon->UpdateOrphan())
	{
		
	header("Location:admin/vieworphan.php?msg=orphanupdatedsucessfully");
	}
	}
?>
 <link href="../css/css.css" rel="stylesheet">

    <div class = "container">
	<div class="wrapperapp">
		<form method="post" 
		class="form-appointment">       
		    <h3 class="form-appointment-heading">Edit Orphan</h3>
			  <hr><br>
			   <input type="text" class="form-control" name="orphan_name" value="<?php echo $orphanid[0]['orphan_name']?>" placeholder="Enter the orphan name" required="" autofocus="" />
                <label data-error="wrong" data-success="right" for="modalLRInput10">Orphan Name</label>

			  <div class="form-group">
                                <div class="input-group">
                                    <input type="radio" name="orphan_gender" value="male" value="<?php echo $orphanid[0]['orphan_gender']?>" checked> Male
                                    <input type="radio" name="orphan_gender" value="<?php echo $orphanid[0]['orphan_gender']?>" value="female"> Female
                                    <input type="radio" name="orphan_gender" value="other"> Other
                                </div>
                            </div>  
                <label data-error="wrong" data-success="right" for="modalLRInput10">Gender</label> 

			  <input type="date" class="form-control" name="orphan_DOB" value="<?php echo $orphanid[0]['orphan_DOB']?>" placeholder="Enter the date" required="" autofocus="" />
                <label data-error="wrong" data-success="right" for="modalLRInput10">DOB</label>

			  <input type="file" class="form-control" name="photo" placeholder="Choose photo" value="<?php echo $orphanid[0]['photo']?>" />   
			  <p><img src="../img/<?php echo $orphanid[0]['photo']; ?>" height="150" width="150"> </p>

			  <button class="btn btn-lg btn-primary btn-block"  name="editorphan" value="editorphan" type="Submit"> Edit Orphan</button> 


</form>
</div>
</div>