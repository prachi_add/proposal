<?php
include('admindash.php');
include('../../controller/orphancontroller.php');

/*session_start();
if(!isset($_SESSION['username']) )
{
  header('location:login.php');
}*/

$orphancon=new orphanController();

if (isset($_POST['addorphan']))
{
	$orphancon->setOrphanName($_POST['orphan_name']);
	$orphancon->setOrphanGender($_POST['orphan_gender']);
	$orphancon->setOrphanDOB($_POST['orphan_DOB']);
	$orphancon-> setPhoto($_POST['photo']);
	if($orphancon->addOrphan())
	{
		
	header("Location:vieworphan.php?msg=orphanaddedsucessfully");
	}
	}
?>
 <link href="../css/css.css" rel="stylesheet">
	<div class="wrapperapp">
		<form method="post" 
		class="form-appointment">       
		    <h3 class="form-appointment-heading">Add Orphan</h3>
			  <hr><br>
			  
			  <input type="text" class="form-control" name="orphan_name" placeholder="Enter the orphan name" required="" autofocus="" />
                <label data-error="wrong" data-success="right" for="modalLRInput10">Orphan Name</label>

			  <div class="form-group">
                                <div class="input-group">
                                    <input type="radio" name="orphan_gender" value="male" checked> Male
                                    <input type="radio" name="orphan_gender" value="female"> Female
                                    <input type="radio" name="orphan_gender" value="other"> Other
                                </div>
                            </div>  
                <label data-error="wrong" data-success="right" for="modalLRInput10">Gender</label> 

			  <input type="date" class="form-control" name="orphan_DOB" placeholder="Enter the date" required="" autofocus="" />
                <label data-error="wrong" data-success="right" for="modalLRInput10">DOB</label>

			  <input type="file" class="form-control" name="photo" placeholder="Choose photo" required=""/>   
                <label data-error="wrong" data-success="right" for="modalLRInput10">Photo</label> 		  
			 
			  <button class="btn btn-lg btn-primary btn-block"  name="addorphan" value="addorphan" type="Submit"> Add Orphan</button>  			
		</form>			
	</div>
