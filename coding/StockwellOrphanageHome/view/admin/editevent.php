<?php
include ('admindash.php');
require ('../../controller/eventcontroller.php');

/*session_start();
if(!isset($_SESSION['username']) )
{
  header('location:login.php');
}*/

$eventcon=new eventController();

$eventid=$eventcon->selectRow($_GET['id']);

if (isset($_POST['editevent']))
{
    $eventcon->setEventId($_GET['id']);
	$eventcon->setEventTitle($_POST['event_title']);
	$eventcon->setDescription($_POST['description']);
	$eventcon->setDate($_POST['date']);
	$eventcon->setTime($_POST['time']);
	$eventcon->setLocation($_POST['location']);
	$eventcon->setDuration($_POST['duration']);

	if($eventcon->UpdateEvent())
	{
		header("Location:viewevent.php?msg=eventupdatedsucessfully");
	}
}
?>


<link href="../css/css.css" rel="stylesheet">
<div class = "container">
	<div class="wrapperapp">
		<form  method="post" name="event" class="form-appointment">       
		    <h3 class="form-appointment-heading">Edit Events</h3>
			  <hr><br>
			  
			  <input type="text" class="form-control" name="event_title" value="<?php echo $eventid[0]['event_title']?>" required="" autofocus="" />
                <label data-error="wrong" data-success="right" for="modalLRInput10">Event title</label>

			  <input type="text" class="form-control"  value="<?php echo $eventid[0]['description']?>" name="description" placeholder="Enter description" required=""/>   
                <label data-error="wrong" data-success="right" for="modalLRInput10">Description</label> 

			  <input type="date" class="form-control" name="date" placeholder="Enter the date" required=""  value="<?php echo $eventid[0]['date']?>" autofocus="" />
                <label data-error="wrong" data-success="right" for="modalLRInput10">Date</label>

			  <input type="time" class="form-control"  value="<?php echo $eventid[0]['time']?>" name="time" placeholder="Enter time" required=""/>   
                <label data-error="wrong" data-success="right" for="modalLRInput10">Time</label> 

			  <input type="text" class="form-control"  value="<?php echo $eventid[0]['location']?>" name="location" placeholder="Enter the location" required="" autofocus="" />

                <label data-error="wrong" data-success="right" for="modalLRInput10"> Location</label>

			  <input type="text" class="form-control"  value="<?php echo $eventid[0]['duration']?>" name="duration" placeholder="Enter duration" required=""/>   
                <label data-error="wrong" data-success="right" for="modalLRInput10">Duration</label>  		  
			 
			  <button class="btn btn-lg btn-primary btn-block"  name="editevent"  type="Submit"> Edit Event</button>  		
		</form>			
	</div>
</div>