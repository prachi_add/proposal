
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Admin Dashboard</title>
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/font-awesome.css" rel="stylesheet">
		<link href="css/datepicker3.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
		
		<!--Custom Font-->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
		
	</head>
	<body>
		<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
					<a class="navbar-brand" href="#"><span>Admin</span>Dashboard</a>
					<ul class="nav navbar-top-links navbar-right">
						
		</nav>
		<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
			<div class="profile-sidebar">
				<div class="profile-userpic">
					<img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
				</div>
				<div class="profile-usertitle">
					<div class="profile-usertitle-name">Admin User</div>
					<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="divider"></div>
			
			<ul class="nav menu">
				<li class="active"><a href="admindash.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
				<li><a href="forum.php"></em> Online Forum</a></li>
				<li><a href="viewevent.php"></em> Events</a></li>
				<li><a href="vieworphan.php"></em> Orphan</a></li>
				<li><a href="viewappointment.php"></em> Appointment</a></li>
				<li><a href="feedback.php"></em> Feedback</a></li>
				<li><a href="adminprofile.php"></em> Profile</a></li>
				<li><a href="viewusers.php"></em> User</a></li>
				<li><a href="../logout.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
			</ul>
			</div>
		</ul>
	</div>
</div>
</nav>

			
			
										<script src="js/jquery-1.11.1.min.js"></script>
										<script src="js/bootstrap.min.js"></script>
										<script src="js/chart.min.js"></script>
										<script src="js/chart-data.js"></script>
										<script src="js/easypiechart.js"></script>
										<script src="js/easypiechart-data.js"></script>
										<script src="js/bootstrap-datepicker.js"></script>
										<script src="js/custom.js"></script>
										<script>
											window.onload = function () {
										var chart1 = document.getElementById("line-chart").getContext("2d");
										window.myLine = new Chart(chart1).Line(lineChartData, {
										responsive: true,
										scaleLineColor: "rgba(0,0,0,.2)",
										scaleGridLineColor: "rgba(0,0,0,.05)",
										scaleFontColor: "#c5c7cc"
										});
										};
										</script>
										
									</body>
								</html>