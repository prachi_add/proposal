<?php
include ('admindash.php');
include('../../controller/eventcontroller.php');

$eventcon=new eventController();

if (isset($_POST['addevent']))
{
	
	$eventcon->setEventTitle($_POST['event_title']);
	$eventcon->setDescription($_POST['description']);
	$eventcon->setDate($_POST['date']);
	$eventcon->setTime($_POST['time']);
	$eventcon->setLocation($_POST['location']);
	$eventcon->setDuration($_POST['duration']);
	if($eventcon->addEvent())
	{
		
	header("Location:viewevent.php?msg=eventaddedsucessfully");
	}
}
?>
 <link href="../css/css.css" rel="stylesheet">

	<div class="wrapperapp">
		<form  method="post" name="event" class="form-appointment">       
		    <h3 class="form-appointment-heading">Add Events</h3>
			  <hr><br>
			  
			  <input type="text" class="form-control" name="event_title" placeholder="Enter the title" required="" autofocus="" />
                <label data-error="wrong" data-success="right" for="modalLRInput10">Event title</label>

			  <input type="text" class="form-control" name="description" placeholder="Enter description" required=""/>   
                <label data-error="wrong" data-success="right" for="modalLRInput10">Description</label> 

			  <input type="date" class="form-control" name="date" placeholder="Enter the date" required="" autofocus="" />
                <label data-error="wrong" data-success="right" for="modalLRInput10">Date</label>

			  <input type="time" class="form-control" name="time" placeholder="Enter time" required=""/>   
                <label data-error="wrong" data-success="right" for="modalLRInput10">Time</label> 

			  <input type="text" class="form-control" name="location" placeholder="Enter the location" required="" autofocus="" />
                <label data-error="wrong" data-success="right" for="modalLRInput10"> Location</label>

			  <input type="text" class="form-control" name="duration" placeholder="Enter duration" required=""/>   
                <label data-error="wrong" data-success="right" for="modalLRInput10">Duration</label>  		  
			 
			  <button class="btn btn-lg btn-primary btn-block"  name="addevent" value="addevent" type="Submit"> Add Event</button>  			
		</form>			
