<div class="nav-links">
		<nav class='navbar navbar-default'>
			<div class='container'>
				<div class='navbar-header'>
					<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
						<span class='sr-only'>Toggle Navigation</span>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
					</button>
				</div>
				
				<div class='collapse navbar-collapse'>
					<ul>
						<li>
							<a href="index.html">Home</a>
						</li>
						<li>
							<a href="#about" class="scroll">About Us</a>
						</li>
						<li>
								<a href="#service" class="scroll">Services</a>
						</li>
						
						
						<li>
							<a href="#gallery" class="scroll">Gallery</a>
						</li>
						<li>
							<a href="#contact" class="scroll">Contact Us</a>
						</li>
						<li>
							<a href="profile.php" >Profile</a>
						</li>
						<li>
							<a href="forum.php" >Forum</a>
						</li>
						<li>
							<a href="logout.php" >Logout</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
	
	<!-- //sticky navigation -->
