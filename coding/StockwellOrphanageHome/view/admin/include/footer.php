<!-- footer -->

	<section class="footer-w3">
		<div class="container">
			<div class="col-lg-4 col-md-4 col-sm-4 footer-agile1" data-aos="zoom-in">
				<h3>Some More</h3>
				<p class="footer-p1">
				</p>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 footer-mid-w3" data-aos="zoom-in">
				<h3>Social Media</h3>
				<div class="agileinfo_footer_grid1">
					<a href="https://www.facebook.com/" target="_blank">
						<img src="images/fb.png" alt=" " class="img-responsive">
					</a>
				</div>
				<div class="agileinfo_footer_grid1">
					<a href="https://www.instagram.com/" target="_blank">
						<img src="images/ig.png" alt=" " class="img-responsive">
					</a>
				</div>
				<div class="agileinfo_footer_grid1">
					<a href="https://www.twitter.com/" target="_blank">
						<img src="images/twitter.png" alt=" " class="img-responsive">
					</a>
				</div>
				
				<div class="clearfix"> </div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 footer-agile1" data-aos="zoom-in">
				<h3>Latest Tweets</h3>
				<ul class="tweet-agile">
					<li>
						<i class="fa fa-twitter-square" aria-hidden="true"></i>
						<p class="tweet-p1">
							<a href="mailto:support@company.com">@shelter</a> Spencer Drama club
							<a href="#">http://sas.sss/se</a>
						</p>
						<p class="tweet-p2">Posted 3 days ago.</p>
					</li>
					<li>
					
						<i class="fa fa-facebook" aria-hidden="true"></i>
						<p class="tweet-p1">
							<a href="mailto:support@company.com">Facebook</a> Drama club
							<a href="#">http://bibek pant.com/fb</a>
						</p>
						<p class="tweet-p2">Posted 3 days ago.</p>
					</li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	
	<!-- copyright -->
	<div class="w3layouts_copy_right">
		<div class="container">
			<p>© 2019 Drama club authority. All rights reserved | Design by
				Bibek
			</p>
		</div>
	</div>
	<!-- //copyright -->
	<!-- //footer -->
