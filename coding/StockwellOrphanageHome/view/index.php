
<!DOCTYPE html>
<html lang="zxx">

<head>
  <title>STOCKWELL ORPHANAGE HOME</title>
  <!--meta tags -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="Organic Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
         Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"
  />
  <script>
    addEventListener("load", function () {
      setTimeout(hideURLbar, 0);
    }, false);


    function hideURLbar() {
      window.scrollTo(0, 1);
    }
  </script>
  <!--booststrap-->
  <link href="web/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">
  <!--//booststrap end-->
  <!-- font-awesome icons -->
  <link href="web/css/font-awesome.min.css" rel="stylesheet">
  <!-- //font-awesome icons -->
  <!--stylesheets-->
  <link href="web/css/style.css" rel='stylesheet' type='text/css' media="all">
  <!--//stylesheets-->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
  <link href="//fonts.googleapis.com/css?family=Patrick+Hand" rel="stylesheet">
  <link href="//fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
</head>

<body>
  <!-- //banner -->
  <div class="banner-left-side" id="home" style="background-image:url(web/images/orphan.jpeg); opacity:2">
    <!-- header -->
    <div class="headder-top">
      <!-- nav -->
      <nav>
        <div id="logo">
          <h1>
            <a href="index.php">SO</a>
          </h1>
        </div>
        <div class="sub-headder position-relative">
          <h6>
            <a href="index.html">STOCKWELL ORPHANAGE
              <br>HOME</a>
          </h6>
        </div>
        <label for="drop" class="toggle">Menu</label>
        <input type="checkbox" id="drop">
        <ul class="menu mt-2">
          <li class="active">
            <a href="index.php">Home</a>
          </li>
          <li>
            <a href="#about">About</a>
          </li>
          <li>
            <a href="#service">Orphan</a>
          </li>
          <li>
            
            <li>
              <a href="#blog">Events</a>
            </li>
            <li>
              <a href="#contact">Contact Us</a>
            </li>
            <a  href="login.php"   class="btn sent-butnn btn-lg" style="border-radius:15px; margin-right:5px;">Login</a>
            <a  href="signup.php"   class="btn sent-butnn btn-lg" style="border-radius:15px; margin-right:5px;">SignUp</a>
        </ul>


      </nav>
      <!-- //nav -->
    </div>
    <!-- //header -->
    <!-- banner -->
    <div class="main-banner text-center">
      <div class="container">
        <div class="social-icons mb-lg-4 mb-3">
          <ul>
            <li class="facebook">
              <a href="#">
                <span class="fa fa-facebook"></span>
              </a>
            </li>
            <li class="twitter">
              <a href="#">
                <span class="fa fa-twitter"></span>
              </a>
            </li>
            <li class="rss">
              <a href="#">
                <span class="fa fa-rss"></span>
              </a>
            </li>
          </ul>
        </div>
        <div class="banner-right-txt">
          <h5 class="mb-sm-3 mb-2">Orphanage Home</h5>
          <h4>Stockwell Orphanage Home</h4>
        </div>
        <div class="slide-info-txt">
          <p>Provide Best Service to Orphans</p>
        </div>
      </div>
    </div>
  </div>
  <!-- //banner -->
  <!-- about -->
  <div class="container">
  <section class="about py-lg-4 py-md-4 py-sm-3 py-3" id="about">
    <h2 class="h1-responsive font-weight-bold text-center my-5">About Us</h2>
  <!-- Section description -->
  <p class="text-center w-responsive mx-auto mb-5">Stockwell Orphanage was founded in 1867 by Charles Spurgeon in what is now Stockwell Park Estate in London. </p>

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-5">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
        <img class="img-fluid" src="web/images/orphan3.jpg" alt="Sample image" >
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-7">
      <!-- Post title -->
     
      <!-- Excerpt -->
      <p>It opened on September 9 1869 and could accommodate 250 children at a cost of about £5,000 a year. The Orphanage was a home for boys without fathers between the ages of six and ten.  In 1879, the orphanage accommodation was expanded allowing girls to be admitted.The orphanage was open until the second World War.</p>
      <p>A warm welcome to the Former Children's Homes website. Having started in 2011, this is the first dedicated encyclopaedia of life in former children's homes and orphanages. Over the years, thousands and thousands of children spent time in these homes and yet we know very little about what life was like in them.</p>
        <p>Over the years, thousands and thousands of children spent time in these homes and yet we know very little about what life was like for them.
      </p>
  </section>
</div>
  <!--//about -->
 
  <!-- orphan -->
  <section class="service py-lg-4 py-md-4 py-sm-3 py-3" id="service">
    <div class="container py-lg-5 py-md-4 py-sm-4 py-3">
      <h3 class="title text-center mb-2">Orphan</h3>
      <div class="title-w3ls-text text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
        <p>Provide home to orphan
        </p>
      </div>
      <div class="row">
         <?php
         require('../controller/orphancontroller.php');
         $pc=new orphanController();
         $data= $pc->selectOrphan();
         foreach($data as $key)
         {
         ?> 
        <div class="col-lg-4 col-md-6 col-sm-6 ser-icon text-center my-3">
          <div class="grid-wthree-service">
            <img src="img/<?php echo $key['photo']?>" height="300px" width="300px" alt="news image" class="img-fluid">
            <div class="ser-text-wthree mt-3">
              <h4>
                <?php echo $key['orphan_name']?>
              </h4>
              <p class="mt-2">GENDER: <?php echo $key['orphan_gender']?></p>
              <p class="mt-2">DOB: <?php echo $key['orphan_DOB']?></p>

            </div>
          </div>
        </div>
       <?php
        }
        ?> 
      </div>
    </div>
  </section>
</div>
</section>
</div>
<
  <!--//service -->
  <!--//vegetable-info -->

  <!-- blog -->
  <section class="blog py-lg-5 py-md-4 py-sm-3 py-3" id="blog">
    <div class="container py-lg-5 py-md-4 py-sm-4 py-3">
     <!--EVENTS-->
<div class="container">
  <h2 class="h1-responsive font-weight-bold text-center my-5">Events</h2>
  <!-- Grid row -->
<div class="row">
  <?php
require('../controller/eventcontroller.php');
$eventcon=new eventController();
$data=$eventcon->selectEvent();
    foreach($data as $key)
    {
  ?> 

<!-- Grid column -->
  <div class="col-lg-4 col-md-6">

    <!--Panel-->
    <div class="card text-center"">
    <div class=" card-header default-color white-text" style="background-color:lightgreen;">
      Events
    </div>
    <div class="card-body">
      <h4 class="card-title"><?php echo $key['event_title']?></h4>
      <p class="card-text"><?php echo $key['description']?></p>
    </div>
    <div class="card-footer text-muted default-color white-text" style="background-color:lightgreen;">
      <p class="mb-0">Date: <?php echo $key['date']?></p>
    </div>
  </div>
  <!--/.Panel-->
</div>
<!-- Grid column -->
  <?php
}
  ?>
</div>
<!-- Grid row -->
</div>
  <center> <button  href="event.php"  type="submit" class="btn sent-butnn btn-lg">View Event</button></center>
    </div>
  </section>
  <!--//blog -->
  
  <!-- contact -->
  <section class="contact py-lg-4 py-md-4 py-sm-3 py-3" id="contact">
    <section>

    <div class="container-fulid">
      <div class="address_mail_footer_grids">
         <iframe width=100% height="500" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.780722534589!2d85.27895471493055!3d27.6931713827978!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb187a97f390b1%3A0xec3f47092df0d4ca!2sKalanki%2C+Kathmandu+44600!5e0!3m2!1sen!2snp!4v1546853194348" >
    </iframe>
      </div>
    </div>
  </section>
    <div class="container py-lg-5 py-md-4 py-sm-4 py-3">
      <h3 class="title text-center mb-2">Get In Touch</h3>
      <div class="title-w3ls-text text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">
        <p>Orphanage Home
        </p>
      </div>
      <div class="contact-form">
        <form action="#" method="post">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 form-group contact-forms">
              <input type="text" class="form-control" placeholder="First Name" required="">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 form-group contact-forms">
              <input type="text" class="form-control" placeholder="Last Name" required="">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 form-group contact-forms">
              <input type="text" class="form-control" placeholder="Phone" required="">

            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 form-group contact-forms">
              <input type="email" class="form-control" placeholder="Email" required="">
            </div>
          </div>
          <div class="form-group contact-forms">
            <textarea class="form-control" placeholder="Meassage" required=""></textarea>
          </div>
          <button  href="login.php"  type="submit" class="btn sent-butnn btn-lg">Send</button>
        </form>
      </div>
    </div>
  </section>
  <!-- footer -->
  <section class="footer-w3layouts-bottem py-lg-4 py-md-3 py-sm-3 py-3">
    <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
      <div class="row ">
        <div class="footer-bottom-info col-lg-4 col-md-4 ">
          <h4 class="pb-lg-4 pb-md-3 pb-3 ">Address</h4>
          <div class="bottom-para ">
            <div class="footer-office-hour">
              <ul>
                <li class="mb-2">
                  <h6>Address</h6>
                </li>
                <li>
                  <p>Kathmandu,Kalanki,
                    <br>Lampati 4101,Nepal.</p>
                </li>
              </ul>
              <ul>
                <li class="my-2">
                  <h6>Phone</h6>
                </li>
                <li>
                  <p>(8087877768)</p>
                </li>
                <li class="my-2">
                  <h6>Email</h6>
                </li>
                <li>
                  <p>
                    <a href="mailto:info@example.com">orphan@gamil.com</a>
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-bottom-info col-lg-4 col-md-4 ">
          <h4 class="pb-lg-4 pb-md-3 pb-3 ">Twitter Us</h4>
          <div class="footer-office-hour">
            <ul>
              <li>
                <p>Best event organizaer</p>
              </li>
              <li class="my-1">
                <p>
                  <a href="mailto:info@example.com">orphan@gamil.com</a>
                </p>
              </li>
              <li class="mb-3">
                <span class="font-italic">Posted 3 days ago.</span>
              </li>
              <li>
                <p>Orpahnage home for orphan provide good service</p>
              </li>
              <li class="my-1">
                <p>
                  <a href="mailto:info@example.com">orphan@gamil.com</a>
                </p>
              </li>
              <li>
                <span class="font-italic">Posted 3 days ago.</span>
              </li>
            </ul>
          </div>
        </div>
        <div class="footer-bottom-info col-lg-4 col-md-4 ">
          <?php
include('../controller/subscribecontroller.php');

$subcon=new subscribeController();

if (isset($_POST['subscribe']))
{
  $subcon->setSEmail($_POST['semail']);
  if($subcon->addSubscriber())
  {
    
  }
  }
?>
          <h4 class="pb-lg-4 pb-md-3 pb-3 ">NewsLetter</h4>
          <div class="newsletter">
            <form  method="post" class="d-flex">
              <input type="email" name="semail" class="form-control" placeholder="Your Email" required="">
              <button class="btn1" name="subscribe" value="subscribe" type="Submit">
                <span class="fa fa-envelope-o" aria-hidden="true"></span>
              </button>
            </form>
          </div>
          <div class="footer-office-hour mt-3">
            <p>Orphanage Home</p>
          </div>
        </div>
      </div>
      <!-- move icon -->
      <div class="text-center mt-lg-5 mt-md-4 mt-3">
        <a href="#home" class="move-top text-center mt-3">
          <span class="fa fa-arrow-up" aria-hidden="true"></span>
        </a>
      </div>
      <!--//move icon -->
    </div>
  </section>
  <!--footer-copy-right -->
  <footer class="bottem-wthree-footer text-center py-md-4 py-3">
    <p>
      © 2019 Orphanage Home. All Rights Reserved | Design by
      <a href="# target="_blank">Prachi</a>
    </p>
  </footer>
  <!--//footer-copy-right -->

</body>

</html>