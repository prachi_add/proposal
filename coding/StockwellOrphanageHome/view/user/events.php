<?php
include('userheader.php');
require('../../controller/eventcontroller.php');
require('../../controller/bookcontroller.php');
require('../../controller/usercontroller.php');

$eventcon=new eventController();
$data=$eventcon->selectEvent();


$bookevent=new bookController();

if (isset($_POST['book']))
{
  $bookevent->setEventId($_GET['eventid']);
  $bookevent->setUserId($_GET['userid']);
  if($bookevent->addBook())
  {
        $_SESSION['success']="Event booked  successfully";
        header("Location:user/events.php");
    }else{
        $_SESSION['error']=" not booked ";
        header("Location: user/events.php");
    }
}
?>

<!--EVENTS-->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="right-container">
  <h2 class="h1-responsive font-weight-bold text-center my-5">Events</h2>
  <!-- Grid row -->
<div class="row">
  <?php
    foreach($data as $key)
    {
  ?> 

<!-- Grid column -->
  <div class="col-lg-4 col-md-6">

    <!--Panel-->
    <div class="card text-center"">
    <div class=" card-header default-color white-text">
      Featured
    </div>
    <div class="card-body">
      <h4 class="card-title"><?php echo $key['event_title']?></h4>
      <p class="card-text"><?php echo $key['description']?></p>
      <p class="card-text">Time: <?php echo $key['time']?></p>
      <p class="card-text">Duration: <?php echo $key['duration']?></p>
      <p class="card-text">Location: <?php echo $key['location']?></p>
      <a class="btn btn-default btn-sm" name="book" href="events.php?id=<?php echo $key['eventid']?>">Book</a>
    </div>
    <div class="card-footer text-muted default-color white-text">
      <p class="mb-0">Date: <?php echo $key['date']?></p>
    </div>
  </div>
  <!--/.Panel-->
</div>
<!-- Grid column -->
  <?php
}
  ?>
</div>
<!-- Grid row -->
</div>
</div>

<hr>
<style>
</style>