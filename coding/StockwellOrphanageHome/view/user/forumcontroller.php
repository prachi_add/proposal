<?php
require_once('../connection.php');
class Forum
{
	
	private $forumId;
	private $userid;
	private $detail;
	private $topic;
	private $questionid;
	private $connect;
	function __construct()
	{
		# code...
		$this->connect = new Connection();
	}
// for setter and getter
	public function setForumId($forumId){
		$this->forumId = $forumId;
	}
	public function getForumId(){
		return $this->forumId;
	}
	public function setUserId($userid){
		$this->userid = $userid;
	}
	public function getUserId(){
		return $this->userIi;
	}
	public function setDetail($detail){
		$this->detail = $detail;
	}
	public function getDetail(){
		return $this->detail;
	}
	public function setTopic($topic){
		$this->topic = $topic;
	}
	public function getTopic(){
		return $this->topic;
	}
	public function setQuestionid($questionid){
		$this->questionid = $questionid;
	}
	public function getQuestionid(){
		return $this->questionid;
	}
		
	public function addQuestion(){
			// setting date and time
			$datetime=date("d/m/y h:i:s"); //create date time
			
			$sql = "INSERT into question(topic,detail, datetime,userid) values ('$this->topic','$this->detail','$datetime',$this->userid)";
			return $this->connect->queries($sql);
		}
	public function getForum(){
		$sql="select * from question";
			return $this->connect->getData($sql);
	}
	
	public function getForumById(){
	$sql="SELECT * from question where questionid=$this->forumId";
	return $this->connect->getData($sql);
	
	}
	public function countComment(){
		$sql= "select count(questionid) from answer where questionid=$this->forumId";
		return $this->connect->getData($sql);
	}
	public function addAnswer(){
			
				$datetime=date("d/m/y h:i:s");
			$sql = "insert into answer(answer,datetime,questionid,userid) values ('$this->detail','$datetime', $this->questionid , $this->userid)";
			return $this->connect->queries($sql);
		}
		public function getAnswer(){
			$sql="SELECT * from answer where questionid=$this->questionid";
				return $this->connect->getData($sql);
		}
		public function updateView($view){
			$sql="update question set view =$view where questionid=$this->forumId";
			return $this->connect->queries($sql);
		}
		public function updateComment($reply){
			$sql="update question set reply =$reply where questionid=$this->questionNo";
			return $this->connect->queries($sql);
		}
}
?>