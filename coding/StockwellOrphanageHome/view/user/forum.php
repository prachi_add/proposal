<?php
session_start();
include "../../controller/usercontroller.php";
include"userheader.php";

$u = new User();

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
            <div class="container">
                <div class="row">
                    <br>
                    <br>

                      <div class="col-md-9">
                         <form method="post">
                            <a href="forum-question.php"
                            " class="btn btn-primary">Start New Discussion</a>

                         </form>
                        <?php
                        include_once('../../controller/forumcontroller.php');
                        include_once('../../controller/usercontroller.php');
                        $forum = new Forum();
                        $rows = $forum->getForum();
                        foreach ($rows as $row) {
                        ?>
                        <a href="forum-detail.php?forumId=<?php echo $row['questionid']; ?>" class="btn btn-default" style="margin-top:2px; width: 100%;">
                            <div class="card">
                                <div class="card-header">
                                    <?php echo $row['topic']; ?>
                                </div>
                                <div class="card-body">
                                    <blockquote class="blockquote mb-0">
                                        <p>
                                            <?php echo $row['detail']; ?>
                                        </p>
                                        <footer class="blockquote-footer">
                                          <?php echo $row['userid']; ?>
                                        </footer>
                                    </blockquote>
                                </div>
                            </div>
                        </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
    </div>

