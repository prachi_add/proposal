<?php
include('userheader.php');
require('../../controller/orphancontroller.php');
require_once ('../../controller/usercontroller.php');
require_once ('../../controller/adoptcontroller.php');
$user=new User();
if(isset($_SESSION['username']) )
{
$userid =User::getID($_SESSION['username']);
$id = $user->selectUser($userid); 
}

$pc=new orphanController();
$data= $pc->selectOrphan();

$adopt=new Adopt();

if (isset($_POST['adopt']))
{
  $bookevent->setEventId($_GET['orphanid']);
  $bookevent->setUserId($_GET['userid']);
  if($adopt->addAdopted())
  {
    }
}
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="container">

	<div class="row">
    <?php
    foreach($data as $key)
    {
      ?>
  		<div class="col-lg-4 ">
			<!--Card-->
      
   			<div class="card card-cascade narrower mb-4" style="margin-top: 28px">

     			 <!--Card image-->
     			<div class="view view-cascade">
        			<img  src="img/<?php echo $key['photo']?>" width="50%" height="200px"class="card-img-top" alt="">
        			<a>
          			<div class="mask rgba-white-slight"></div>
        			</a>
      		   	</div>
     	 <!--/.Card image-->

     	 <!--Card content-->
      			<div class="card-body card-body-cascade">
        			<!--Title-->
        			<h4 class="card-title"><?php echo $key['orphan_name']?></h4>
        		<!--Text-->
       			 	<p class="card-text"><?php echo $key['orphan_gender']?></p>
              <p class="card-text"><?php echo $key['orphan_DOB']?></p>
        			<a class="btn btn-default" href="appointment.php?orphanid=<?php echo $key['orphanid']?>" type="submit">Adopt</a>
      			</div>
            
     			 <!--/.Card content-->
     		</div>
    	<!--/.Card-->
      		</div>
		<?php
        }
    ?>

</div>
</div>
</div>



