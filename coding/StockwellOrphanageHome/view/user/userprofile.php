<?php
session_start();
include('userheader.php');
require_once ('../../controller/usercontroller.php');
$user=new User();
if(isset($_SESSION['username']) )
{
$userid =User::getID($_SESSION['username']);
$id = $user->selectUser($userid); 
}

if(isset($_POST['editprofile']))
{
$user->setUserId($userid);
$user->setFirstname($_POST['first_name']);
$user->setLastname($_POST['last_name']);
$user->setEmail($_POST['email']);
$user->setDOB($_POST['DOB']);
$user->setAddress($_POST['address']);
$user->setPhone($_POST['phone']);
$user->setOccupation($_POST['occupation']);
$user->setGender($_POST['gender']);
$user->setUsername($_POST['username']);
$user->setPassword($_POST['password']);
if($user->editprofile()){
$string = "
<script>
alert('Profile has been edited');
</script>
";
echo $string;
header("location:userprofile.php");
}
}
?>
<div class="container">
<div class="container">
<div class="row">
    <div class="col-lg-5"></div>
<div class="col-lg-2">
<br>
<br>
 </div>  
    <div class="col-lg-5"></div></div>
</div>
<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Update Profile</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" >
                            <fieldset>
                            <div class="form-group">
                                    <input type="text" class="form-control" name="first_name" value="<?php echo $id[0]['first_name']?>"  autofocus="autofocus" required pattern="^[A-Za-z]+" placeholder="First Name" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="last_name" value="<?php echo $id[0]['last_name']?>" autofocus="autofocus" required pattern="^[A-Za-z]+" placeholder="Last Name" required="">
                                </div>
                                <div class="form-group">
                                   <input type="email"  class="form-control" name="email" id="email" value="<?php echo $id[0]['email']?>"  placeholder="Email" required="">
                                </div>
                                <div class="form-group">
                                    
                                    <input type="date" onchange class="form-control" name="DOB" value="<?php echo $id[0]['DOB']?>"  autofocus="autofocus" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="address" id="address" value="<?php echo $id[0]['address']?>"  placeholder=" Address"/>
                                </div>
                                <div class="form-group">
                                   <input type="number" value="<?php echo $id[0]['phone']?>" class="form-control" name="phone" placeholder=" Phone Number">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="occupation" id="occupation" value="<?php echo $id[0]['occupation']?>" placeholder=" Occupation"/>
                                </div>
                                <div class="form-group">
                                <div class="input-group" value="<?php echo $id[0]['gender']?>">
                                                        <input type="radio" name="gender" value="male" checked> Male
                                                        <input type="radio" name="gender" value="female"> Female
                                                        <input type="radio" name="gender" value="other"> Other
                                                    </div>
                                </div>
                                <div class="form-group">
                                   <input type="text" class="form-control" name="username" id="username" value="<?php echo $id[0]['username']?>" placeholder="Username" required pattern="^[A-Za-z0-9]+" required="">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="password" value="<?php echo $id[0]['password']?>"  placeholder=" Password"  required="">
                                </div>
                                
                                
                                  <button class="btn btn-success btn-block" name="editprofile"> Update Profile</button>                           
                            </fieldset>
                            
                            <hr>                       
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<style>
.input-group
{
 color:white; 
}
.container
{
    position:center;
}
</style>


<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/bootstrap.js"></script>