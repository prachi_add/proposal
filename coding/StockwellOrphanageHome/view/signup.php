<?php
include('link.php');
include_once ('../controller/usercontroller.php');

$user = new User();

if(!empty(isset($_POST['register'])))
{    
    $user->setFirstname($_POST['first_name']);
    $user->setLastname($_POST['last_name']);
    $user->setEmail($_POST['email']);
    $user->setDOB($_POST['DOB']);
    $user->setAddress($_POST['address']);
    $user->setPhone($_POST['phone']);
    $user->setOccupation($_POST['occupation']);
    $user->setGender($_POST['gender']);
    $user->setUsername($_POST['username']);
    $user->setPassword($_POST['password']);
    $user->setUserType($_POST['subscriber']);
    
    
    if($user->register()){
        $_SESSION['register_success'] = "User created successfully";
        $message = "User created successfully";
        header("Location:login.php");
    }else{

        $_SESSION['register_error'] = "User not created ";
        header("Location:signup.php");
    }
}

?>

<div class="container">
<div class="container">
<div class="row">
    <div class="col-lg-5"></div>
<div class="col-lg-2">
<br>
<br>
 </div>  
    <div class="col-lg-5"></div></div>
</div>
<div class="container">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Sign Up for Free</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="">
                            <!-- displaying error messages -->

                        <?php if(!empty($_SESSION['register_success'])){ 
                            ?>
                             <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-error alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                  <?php echo $_SESSION['register_success']; ?>
                                    </div>
                                </div>       
                            </div>

                            <?php } ?>
                            
                            <!--displaying message for success  -->
                            
                            <?php if(!empty($_SESSION['register_error'])){ 
                            ?>
                             <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                  <?php echo $_SESSION['register_error']; ?>
                                    </div>
                                </div>       
                            </div>

                            <?php } ?>

                            <fieldset>
                            <div class="form-group">
                                    <input type="text" class="form-control" name="first_name" id="first_name"  autofocus="autofocus" required pattern="^[A-Za-z]+" placeholder="First Name" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="last_name" id="last_name" autofocus="autofocus" required pattern="^[A-Za-z]+" placeholder="Last Name" required="">
                                </div>
                                <div class="form-group">
                                   <input type="email"  class="form-control" name="email" id="email"  placeholder="Email" required="">
                                </div>
                                <div class="form-group">
                                    
                                    <input type="date" onchange class="form-control" name="DOB"  autofocus="autofocus" required="">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="address" id="address"  placeholder=" Address"/>
                                </div>
                                <div class="form-group">
                                   <input type="number" class="form-control" name="phone" placeholder=" Phone Number">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="occupation" id="occupation"  placeholder=" Occupation"/>
                                </div>
                                <div class="form-group">
                                <div class="input-group">
                                                        <input type="radio" name="gender" value="male" checked> Male
                                                        <input type="radio" name="gender" value="female"> Female
                                                        <input type="radio" name="gender" value="other"> Other
                                                    </div>
                                </div>
                                <div class="form-group">
                                   <input type="text" class="form-control" name="username" id="username"  placeholder="Username" required pattern="^[A-Za-z0-9]+" required="">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" id="password"  placeholder=" Password"  required="">
                                </div>
                                
                                
                                  <button class="btn btn-success btn-block" name="register"> Register</button>                           
                            </fieldset>
                            
                            <hr>
                            <p><a href="login.php">Already have account </a></p>                       
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<style>
.input-group
{
 color:white; 
}
.container
{
    position:center;
}
body
{
    //background-image:url(img/orphan5.jpeg);
}
</style>

    <script src="js/jquery-2.2.3.min.js"></script>
<script src="js/bootstrap.js"></script>