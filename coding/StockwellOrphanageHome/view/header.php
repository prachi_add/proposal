<!docType html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml" >

<head>
 
<title>Stockwell Orphanage Home</title>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min">
<link rel="stylesheet" href="bootstrap/css/bootstrap">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="bootstrap/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="bootstrap/css/style.min.css" rel="stylesheet">
    
</head>

<body>
	<!--Navbar-->
<nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
  

  <!-- Navbar brand -->
  <a class="navbar-brand" href="home.php"><img  src="img/logo.png"></a>

  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="aboutus.php">About Us</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link"  href="orphans.php">Orphans</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="events.php">Events</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="forum.php">Forum</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contactus.php">Contact Us</a>
      </li>
      

    </ul>
    <form class="form-inline">
      <div class="md-form my-0">
        <a href="signup.php" class="btn btn-default btn-rounded my-3" > SignUp</a>
        <a href="login.php" class="btn btn-default btn-rounded my-3" > LogIn</a>
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
      </div>
    </form>
  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->
<style>
body
{
  background-color:#E6E6FA;
}

.navbar
{
	font-color:white;
}
.login_btn{
color: black;
background-color: #FFC312;
width: 100px;
margin-top:13px;
}

.login_btn:hover{
color: black;
background-color: white;
}
.navbar-brand img
{
  height:50px;
  width:50px;
  border-radius:50px; 
}
</style>
</body>